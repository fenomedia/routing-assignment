import React, { Component } from 'react';
import { BrowserRouter, Route, NavLink, Switch, Redirect } from 'react-router-dom';
import styles from './App.css';

import Courses from './containers/Courses/Courses';
import Users from './containers/Users/Users';

class App extends Component {
  render () {
    return (
      <BrowserRouter>
          <div className={styles.App}>
            <NavLink className="NavLink" to="/users">Users</NavLink>
            <NavLink className="NavLink" to="/courses">Courses</NavLink>
            <NavLink className="NavLink" to="/all-courses">Your Courses</NavLink>
            <Switch>
              <Redirect from="/all-courses" to="/courses"/>
              <Route path='/users' component={Users}/>
              <Route path='/courses' component={Courses}/>
              <Route render={() => <h1>Not found</h1> } />
            </Switch>
          </div>
      </BrowserRouter>
    );
  }
}

export default App;
